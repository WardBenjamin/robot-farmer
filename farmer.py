#!/usr/bin/env python
#
# Robot Farmer - by Benjamin Ward
# (C) 2014 Benjamin Ward
# All rights reserved

__author__ = 'Benjamin Ward'
__version__ = '0.1'
__build__ = 'dev'

from os import sep
from load_assets import load_png
from paths import imagedir
import robotstats
from robotstats import velocity
import pygame

class Robot(object):
    def __init__(self, parent_screen, pos):
        self.images = {
            'front'  : load_png(imagedir + sep + 'robot_front_small.png'),
            'back'   : load_png(imagedir + sep + 'robot_back_small.png'),
            'left'  : load_png(imagedir + sep + 'robot_left_small.png'),
            'right'  : load_png(imagedir + sep + 'robot_right_small.png')

        }
        self.image = self.images['front']
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.velocity = velocity()
        self.parent_screen = parent_screen
        parent_screen.renderables.append(self)
        parent_screen.updateables.append(self)

    def handle_movement(self):
        key = pygame.key.get_pressed()
        if key[pygame.K_LEFT]:
            self.velocity.x = robotstats.speed * -1
            self.image = self.images['left']
        if key[pygame.K_RIGHT]:
            self.velocity.x = robotstats.speed
            self.image = self.images['right']
        if key[pygame.K_UP]:
            self.velocity.y = robotstats.speed * -1
            self.image = self.images['back']
        if key[pygame.K_DOWN]:
            self.velocity.y = robotstats.speed
            self.image = self.images['front']

        if not key[pygame.K_LEFT] and not key[pygame.K_RIGHT]:
            self.velocity.x = 0

        if not key[pygame.K_UP] and not key[pygame.K_DOWN]:
            self.velocity.y = 0

    def update(self):
        self.handle_movement()
        self.rect.y += self.velocity.y

        for block in self.parent_screen.blocks:
            if self.rect.colliderect(block.rect):
                if self.velocity.y > 0: # Moving down; Hit the top side of the block
                    self.rect.bottom = block.rect.top
                    self.velocity.y = 0
                    self.onGround = True
                elif self.velocity.y < 0: # Moving up; Hit the bottom side of the block
                    self.rect.top = block.rect.bottom
                    self.velocity.y = 0

        """for mob in self.parent_screen.mobs: # Hit a monster
            if self.rect.colliderect(mob.rect):
                if self.velocity.y > 0: # Moving down; Hit the top side of the monster
                    self.rect.bottom = mob.rect.top
                    self.velocity.y = 0
                elif self.velocity.y < 0: # Moving up; Hit the bottom side of the monster
                    self.rect.top = mob.rect.bottom
                    self.velocity.y = 0
        """

        self.rect.x += self.velocity.x

        for block in self.parent_screen.blocks:
            if self.rect.colliderect(block.rect):
                if self.velocity.x > 0: # Moving right; Hit the left side of the block
                    self.rect.right = block.rect.left
                    self.velocity.x = 0
                elif self.velocity.x < 0: # Moving left; Hit the right side of the block
                    self.rect.left = block.rect.right
                    self.velocity.x = 0


        """for monster in constant.monsters: # Hit a monster
            if self.rect.colliderect(monster.rect):
                if self.hitTimer <= 0:
                    self.stats.health -= monster.damage
                monster.health -= self.stats.damage
                if self.velocity.x > 0: # Moving right; Hit the left side of the monster
                    self.rect.right = monster.rect.left
                    self.velocity.x = 0
                elif self.velocity.x < 0: # Moving left; Hit the right side of the monster
                    self.rect.left = monster.rect.right
                    self.velocity.x = 0
        """

    def draw(self, display):
        display.blit(self.image, self.rect)
