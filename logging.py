#!/usr/bin/env python
#
# Robot Farmer - by Benjamin Ward
# (C) 2014 Benjamin Ward
# All rights reserved

__author__ = 'Benjamin Ward'
__version__ = '0.1'
__build__ = 'dev'

class InformationPost(object):
    def __init__(self, message):
        self.message = "[Info]" + message
        print(self.message)


class InformationLog(InformationPost):
    def __init__(self, message):
        super().__init__(message)


class ErrorPost(object):
    def __init__(self, message):
        self.message = "[Error]" + message
        print(self.message)
        del self

class ErrorLog(ErrorPost):
    def __init__(self, message):
        super().__init__(message)
        del self
