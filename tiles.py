#!/usr/bin/env python
#
# Robot Farmer - by Benjamin Ward
# (C) 2014 Benjamin Ward
# All rights reserved

__author__ = 'Benjamin Ward'
__version__ = '0.1'
__build__ = 'dev'

from load_assets import load_png
from paths import imagedir
from os import sep

class Tile(object):
    def _init__(self):
        self.tile_type = 'tile'

class FarmTile(Tile):
    def __init__(self, pos):
        self.tile_type = 'farm_untilled'
        self.tilled_val = 0
        self.walked_val = 0
        self.images = {
            'grass'    : load_png(imagedir + sep + 'grass_tile.png'),
            'tilled1'  : load_png(imagedir + sep + 'tilled1_tile.png'),
            'tilled2'  : load_png(imagedir + sep + 'tilled2_tile.png'),
            'tilled3'  : load_png(imagedir + sep + 'tilled_tile.png')
        }
        self.image = self.images['grass']
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]

    def draw(self, surface):
        surface.blit(self.image, self.rect)

    def update(self): pass

    def till(self):
        self.image = self.images['tilled3']
        self.tile_type = 'farm_tilled'