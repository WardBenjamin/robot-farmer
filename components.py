#!/usr/bin/env python
#
# Robot Farmer - by Benjamin Ward
# (C) 2014 Benjamin Ward
# All rights reserved

__author__ = 'Benjamin Ward'
__version__ = '0.1'
__build__ = 'dev'

import pygame
from fonts import monospace15Font
from load_assets import load_png
from paths import imagedir
from os import sep


class Component(object):
    def __init__(self, pos, width, height):

        self.x = pos[0]
        self.y = pos[1]
        self.width = width
        self.height = height

        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)
    
    def render(self, surface):
        raise NotImplementedError
    
    def update(self):
        raise NotImplementedError
    
class Button(Component):
    def __init__(self, text, pos, width, height, color):
        super().__init__(pos, width, height)
        self.text = text
        self.color = color
        self.images = {
            'down' : load_png(imagedir + sep + 'button_down.png'),
            'up'   : load_png(imagedir + sep + 'button_up.png')
        }
        self.image = pygame.Surface(width, height)
        self.image.fill(color)

    def render(self, surface):
        surface.blit(self.image, self.rect)

        label = monospace15Font.render(self.text, 1, (255,255,0))
        surface.blit(label, (self.x + (self.width / 2) - ((monospace15Font.size(self.text)[0]) / 2), self.y +
                                                    (self.height / 2) - ((monospace15Font.size(self.text)[1]) / 2)))
        
    def update(self):
        pass

    def clicked(self):
        pass

class MenuButton(Button):
    def __init__(self, pos, width, height, color, parent):
        super().__init__("Menu", pos, width, height, color)
        self.parent_screen = parent
        self.parent_game = parent.parent
        self.parent_screen.renderables.append(self)
        self.parent_screen.clickables.append(self)

    def clicked(self):
        self.parent_game.dominant_screen = 'menu'