#!/usr/bin/env python
#
# Robot Farmer - by Benjamin Ward
# (C) 2014 Benjamin Ward
# All rights reserved

__author__ = 'Benjamin Ward'
__version__ = '0.2'
__build__ = 'dev'

import sys
import pygame
from pygame.locals import *

pygame.init()

from screens import MenuScreen, FarmScreen, StoreScreen
from dimensions import ScreenX, ScreenY
from fonts import fpsFont
from colors import color

class Game(object):
    def __init__(self):
        self.generate_display()
        self.dominant_screen = 'farm'
        self.menu_screen = MenuScreen(self)
        self.farm_screen = FarmScreen(self)
        self.store_screen = StoreScreen(self)
        self.clock = pygame.time.Clock()
        self.running = True
        self.game_loop()

    def generate_display(self):
        self.display = pygame.display.set_mode((ScreenX, ScreenY))
        pygame.display.set_caption('Robot Farmer ' + __build__ + ' v' +__version__)

    def switch_screens(self):
        if self.dominant_screen == 'farm':
            self.dominant_screen = 'store'
        elif self.dominant_screen == 'store':
            self.dominant_screen = 'farm'

    def go_to_menu(self):
        self.dominant_screen = 'menu'

    def send_click_to_dominant_screen(self, clickRect):
        if self.dominant_screen == 'menu':
            self.menu_screen.check_click_effects(clickRect)
        elif self.dominant_screen == 'farm':
            self.farm_screen.check_click_effects(clickRect)
        elif self.dominant_screen == 'store':
            self.store_screen.check_click_effects(clickRect)




    def game_loop(self):
        while self.running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.running = False
                elif event.type == KEYDOWN:
                    if event.key == K_LEFTBRACKET:
                        self.switch_screens()
                    elif event.key == K_RIGHTBRACKET:
                        self.switch_screens()
                    elif event.key == K_ESCAPE:
                        pygame.event.post(pygame.event.Event(QUIT))
                elif event.type == MOUSEBUTTONDOWN:
                    mousePos = event.pos
                    rect = pygame.Rect(0, 0, 1, 1)
                    rect.x, rect.y = mousePos
                    self.send_click_to_dominant_screen(rect)

            self.update()
            self.render()
            self.clock.tick(60)
            pygame.display.update()

        pygame.quit()
        sys.exit()


    def render(self):
        if self.dominant_screen == 'menu':
            self.menu_screen.draw(self.display)
        elif self.dominant_screen == 'farm':
            self.farm_screen.draw(self.display)
        elif self.dominant_screen == 'store':
            self.store_screen.draw(self.display)

        fpsLabel = fpsFont.render("FPS: " + str(self.fps), True, color['black'])
        fpsRect = fpsLabel.get_rect()
        fpsRect.bottom = ScreenY
        self.display.blit(fpsLabel, fpsRect)

    def update(self):
        if self.dominant_screen == 'menu':
            self.menu_screen.update()
        elif self.dominant_screen == 'farm':
            self.farm_screen.update()
        elif self.dominant_screen == 'store':
            self.store_screen.update()

        self.fps = self.clock.get_fps()

if __name__ == '__main__':
    Game()