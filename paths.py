#!/usr/bin/env python
#
# Robot Farmer - by Benjamin Ward
# (C) 2014 Benjamin Ward
# All rights reserved

__author__ = 'Benjamin Ward'
__version__ = '0.1'
__build__ = 'dev'

from os.path import dirname
from os import sep

curdir = dirname(__file__)
imagedir = curdir + sep + 'images'