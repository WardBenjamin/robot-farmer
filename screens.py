#!/usr/bin/env python
#
# Robot Farmer - by Benjamin Ward
# (C) 2014 Benjamin Ward
# All rights reserved

__author__ = 'Benjamin Ward'
__version__ = '0.1'
__build__ = 'dev'

import pygame
from os import sep
from dimensions import ScreenX, ScreenY
from load_assets import load_png
from paths import imagedir
from farmer import Robot
from tiles import FarmTile
from logging import InformationLog

class Screen(object):
    # A constructor for screen objects.
    # Note: all functions are the same for all screen classes
    # except the __init__() function. All screens must inherit 4
    # from this class, then call super().__init__().
    def __init__(self, width=ScreenX, height=ScreenY):
        self.surface = pygame.Surface((width, height)) #.convert()
        self.updateables = []
        self.renderables = []
        self.clickables = []
        self.bkg_name = 'default_bkg.png'

    def load_background(self):
        self.background = load_png(imagedir + sep + self.bkg_name)

    def render(self):
        self.surface.blit(self.background, (0, 0))
        for object in self.renderables:
            try:
                object.draw(self.surface)
            except AttributeError as message:
                raise SystemExit(message)

    def update(self):
        for obj in self.updateables:
            obj.update()
        self.render()
        self.apply_key_effects()
        self.check_collision()

    def check_click_effects(self, clickRect):
        pass

    def apply_key_effects(self):
        raise NotImplementedError

    def check_collision(self):
        raise NotImplementedError

    def draw(self, display):
        display.blit(self.surface, (0, 0))

class MenuScreen(Screen):
    def __init__(self, parent):
        super().__init__()
        self.bkg_name = 'menu_bkg.png'
        self.load_background()
        self.parent = parent

    def apply_key_effects(self):
        pass

    def check_click_effects(self, clickRect):
        pass

    def check_collision(self):
        pass

class FarmScreen(Screen):
    def __init__(self, parent):
        super().__init__()
        self.tiles = []
        self.blocks = []
        self.load_background()
        self.robot = Robot(self, (0, 0))
        self.parent = parent

    def load_background(self):
        tiles_x = ScreenX//40
        tiles_y = ScreenY//40
        self.background = pygame.Surface((ScreenX, ScreenY))
        def draw_row_tiles(y):
            x = 0
            for i in range(tiles_x):
                i = FarmTile((x, y))
                self.renderables.append(i)
                self.updateables.append(i)
                self.tiles.append(i)
                self.background.blit(i.image, i.rect)
                x += 40

        y = 0
        for i in range(tiles_y):
            draw_row_tiles(y)

    def check_click_effects(self, clickRect):
        for obj in self.clickables:
            if obj.rect.collliderect(clickRect):
                obj.clicked()

    def apply_key_effects(self):
        key = pygame.key.get_pressed()
        if key[pygame.K_SPACE]:
            for tile in self.tiles:
                if tile.rect.collidepoint(self.robot.rect.center) and tile.tile_type == 'farm_untilled':
                    tile.till()

    def check_collision(self):
        for tile in self.tiles:
            if tile.rect.collidepoint(self.robot.rect.center):
                tile.walked_val += 1

class StoreScreen(Screen):
    def __init__(self, parent):
        super().__init__()
        self.bkg_name = 'shop_bkg.png'
        self.load_background()
        self.parent = parent

    def check_click_effects(self, clickRect):
        for obj in self.clickables:
            if obj.rect.collliderect(clickRect):
                obj.clicked()

    def apply_key_effects(self):
        pass

    def check_collision(self):
        pass
